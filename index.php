<?php
require_once __DIR__.'\..\config.inc.php';

const FORMAT = 'Y-m-d H:i';

const PHONE = ''; // phone number
const API_KEY = ''; // api key
const APP_NAME = ''; // app name
const TEMPLATE_ID = ''; // template id

const ERROR_PATH = __DIR__.'\logs\error.json';
const SUCCESS_PATH = __DIR__.'.\logs\success.json';

$db_name = $dbconfig['db_name'];
$db_server = $dbconfig['db_server'];
$db_username = $dbconfig['db_username'];
$db_password = $dbconfig['db_password'];
$pdo = new PDO("mysql:dbname=$db_name;host=$db_server;charset=utf8", $db_username, $db_password);

////// Запрос на время проведения встречи

$is_item_q = "SELECT 
acrel.activityid AS id,
ac.date_start AS start_date,
ac.time_start AS start_time,
ac.due_date AS end_date,
ac.time_end AS end_time,

ac.location,
ac.subject AS theme,

crm2.setype AS type,
crm2.crmid AS sec_id,

right(replace(us2.phone_mobile, ' ', ''), 9) AS phone_own,
REPLACE(CONCAT_WS(' ', us.first_name, us.last_name), ' ', '') AS creator_name

FROM vtiger_seactivityrel AS acrel
INNER JOIN vtiger_activity AS ac ON ac.activityid = acrel.activityid
INNER JOIN vtiger_crmentity AS crm ON crm.crmid = ac.activityid
INNER JOIN vtiger_crmentity AS crm2 ON crm2.crmid = acrel.crmid
INNER JOIN vtiger_users AS us ON us.id = crm.smcreatorid
INNER JOIN vtiger_users AS us2 ON us2.id = crm.smownerid
WHERE ac.activitytype = 'meeting' AND crm.deleted = 0 AND ac.eventstatus != 'Выполнено' AND ac.date_start in (:today, :tomorrow)";

$is_item_r = get_data($pdo, $is_item_q, [':today'=>date('Y-m-d'), ':tomorrow'=>date('Y-m-d', strtotime('+1 day'))]);
	
foreach ($is_item_r as $item) {
	$error_mess = '';
	if ( !validate_phone($item['phone_own']) ) {
		$error_mess = 'Phone invalid';
	}

	$phone = '996'.$item['phone_own'];

	$start_date = date(FORMAT, strtotime($item['start_date'].$item['start_time']));
	$end_date = date(FORMAT, strtotime($item['end_date'].$item['end_time']));
	$def_date = date(FORMAT, strtotime('-1 hour', strtotime($start_date)));
	$cur_time = date(FORMAT);

	$json_log = json_decode(file_get_contents(SUCCESS_PATH), true);
	foreach ($json_log as $js_key => $js_item) {
		if ($js_item['id'] == $item['id']) {
			$error_mess = 'Already sent';
		}
	}

	if ($error_mess != '') {
		put_error(ERROR_PATH, $item['id'], $error_mess, $cur_time);
		continue;
	}

	if ($item['location'] == '') $item['location'] = 'Не указано';

	if ( get_closest_time($cur_time, $def_date) ) {
		$curl2 = curl_init();

	    curl_setopt_array($curl2, array(
	      CURLOPT_URL => 'https://api.gupshup.io/sm/api/v1/app/opt/in/'.APP_NAME,
	      CURLOPT_RETURNTRANSFER => true,
	      CURLOPT_ENCODING => '',
	      CURLOPT_MAXREDIRS => 10,
	      CURLOPT_TIMEOUT => 0,
	      CURLOPT_FOLLOWLOCATION => true,
	      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	      CURLOPT_CUSTOMREQUEST => 'POST',
	      CURLOPT_POSTFIELDS => 'user=' . $phone,
	      CURLOPT_HTTPHEADER => array(
	        'apikey: ' . API_KEY,
	        'Content-Type: application/x-www-form-urlencoded'
	      ),
	    ));

	    curl_exec($curl2);
	    curl_close($curl2);

		try{
		 	$curl = curl_init();

			curl_setopt_array($curl, array(
			    CURLOPT_URL => 'http://api.gupshup.io/sm/api/v1/template/msg',
			    CURLOPT_RETURNTRANSFER => true,
			    CURLOPT_ENCODING => '',
			    CURLOPT_MAXREDIRS => 10,
			    CURLOPT_TIMEOUT => 0,
			    CURLOPT_FOLLOWLOCATION => true,
			    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			    CURLOPT_CUSTOMREQUEST => 'POST',
			    CURLOPT_POSTFIELDS => 'destination=' . $phone .
			    '&source=' . PHONE . 
			    '&template={"id": ' . TEMPLATE_ID .',"params": ["' . 
			    $item['theme'] . '","' . 
			    get_name($item['type'], $item['sec_id'], $pdo) . '","' . 
			    $item['location'] . '","' . 
			    $start_date . '","' . 
			    $end_date . '","' . 
			    $item['creator_name'] .'"]}',
			    CURLOPT_HTTPHEADER => array(
			      'apikey: ' . API_KEY,
			      'Content-Type: application/x-www-form-urlencoded'
			    ),
			));

			$response = json_decode(curl_exec($curl), true);
			curl_close($curl);
			print_r($response);

			if( curl_errno($curl) ) {
			    throw new \Exception(curl_error($curl));
			} else if (is_array($response) && $response['status'] == 'error') {
				throw new \Exception($response['message']);
			}

			$json_log[] = array('id'=>$item['id'], 'date'=>$cur_time, 'start_date'=>$start_date, 'name'=>get_name($item['type'], $item['sec_id'], $pdo));
			put_data(SUCCESS_PATH, json_encode($json_log));

		}catch(Exception $e){
			put_error(ERROR_PATH, $item['id'], $e->getMessage(), date(FORMAT));
		}

	}
}

function get_data ($pdo, $sql, $params = null) {
	if ($params && !is_array($params)) return;
	$init = $pdo->prepare($sql);
	$init->execute($params);
	$res = $init->fetchAll(PDO::FETCH_ASSOC);

	return $res;
}

function get_closest_time ($time, $deftime) {
	for ($i = 0; $i < 6; $i++) {
		if (date(FORMAT, strtotime("+$i minutes", strtotime($time))) == $deftime) {
			return true;
		}
	}
	return false;
}

function put_data ($path, $data) {
	if (!file_exists($path)) return "file doesn't exists";
	file_put_contents($path, $data);
}		

function validate_phone ($phone) {
	$num_phone = intval($phone);
	if (strlen($num_phone) == 9) {
		return true;
	}
	return false;
}

function put_error ($path, $id, $message, $date) {
	$json = json_decode(file_get_contents($path), true);
	foreach ($json as $key => $value) {
		if ($value['id'] == $id && $message == $value['message']) {
			return;
		}
	}
	$json[] = array('id'=>$id, 'message'=>$message, 'date'=>$date);
	put_data($path, json_encode($json))	;
}

function get_name ($type, $id, $pdo) {
	if ($type == 'Leads') {
		$query = "SELECT CONCAT_WS(' ', firstname, lastname) AS name FROM vtiger_leaddetails
INNER JOIN vtiger_crmentity AS crm ON crm.crmid = vtiger_leaddetails.leadid
WHERE leadid = :id AND crm.deleted = 0";
	} else if ($type == 'Accounts') {
		$query = "SELECT acc.accountname as name FROM vtiger_account AS acc
INNER JOIN vtiger_crmentity AS crm ON crm.crmid = acc.accountid
WHERE crm.deleted = 0 AND acc.accountid = :id";
	}

	$res = get_data($pdo, $query, array('id' => $id));

	return $res[0]['name'];
}
